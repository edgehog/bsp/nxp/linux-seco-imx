#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <linux/clk.h>
#include <sound/pcm_params.h>

#include "../codecs/alc655.h"
#include "imx-audmux.h"
#include "fsl_ssi.h"

#define DRV_NAME "imx-ac97-alc655"
#define DAI_NAME_SIZE	32

struct imx_alc655_data {
	struct snd_soc_dai_link dai;
	struct snd_soc_card     card;
	char                    codec_dai_name[DAI_NAME_SIZE];
	char                    platform_name[DAI_NAME_SIZE];
	struct clk              *codec_clk;
	unsigned int            clk_frequency;
};


static int imx_alc655_dai_init(struct snd_soc_pcm_runtime *rtd) {
	return 0;
}


static int imx_alc655_audio_params (struct snd_pcm_substream *substream,
        struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = asoc_substream_to_rtd(substream);
	struct snd_soc_dai *codec_dai = asoc_rtd_to_cpu(rtd, 0);
	struct snd_soc_dai *cpu_dai = asoc_rtd_to_cpu(rtd, 1);
	struct snd_soc_card *card = rtd->card;
	struct device *dev = card->dev;
	unsigned int fmt;
	int ret = 0;

	// struct snd_soc_pcm_runtime *rtd = substream->private_data;
	// struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	// int ret;

	fmt = SND_SOC_DAIFMT_AC97 | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFS;
	ret = snd_soc_dai_set_fmt( cpu_dai, fmt );
	if ( ret ) {
		dev_err( dev, "failed to set cpu dai fmt: %d\n", ret );
		return ret;
	}

	ret = snd_soc_dai_set_fmt(codec_dai, fmt);

	ret = snd_soc_dai_set_tdm_slot(cpu_dai, 0, 0, 2,
									params_physical_width(params));
	if (ret) {
			dev_err(dev, "failed to set cpu dai tdm slot: %d\n", ret);
			return ret;
	}
	ret = snd_soc_dai_set_sysclk(cpu_dai, 0, 0, SND_SOC_CLOCK_IN);

    return ret;
}


static int imx_audmux_ac97_config (struct platform_device *pdev, int intPort, int extPort) {
	int ret;
	unsigned int ptcr, pdcr;

	intPort = intPort - 1;
	extPort = extPort - 1;

	ptcr = IMX_AUDMUX_V2_PTCR_SYN | IMX_AUDMUX_V2_PTCR_TCLKDIR | IMX_AUDMUX_V2_PTCR_TCSEL(extPort);
	pdcr = IMX_AUDMUX_V2_PDCR_RXDSEL(extPort);

	ret = imx_audmux_v2_configure_port (intPort, ptcr, pdcr);
	if ( ret ) {
		dev_err (&pdev->dev, "Audmux internal port setup failed\n");
		return ret;
	}

	ptcr = IMX_AUDMUX_V2_PTCR_SYN | IMX_AUDMUX_V2_PTCR_TFSDIR | IMX_AUDMUX_V2_PTCR_TFSEL(intPort);

	pdcr = IMX_AUDMUX_V2_PDCR_RXDSEL (intPort);

	ret = imx_audmux_v2_configure_port (extPort, ptcr, pdcr);
	if ( ret ) {
		dev_err (&pdev->dev, "Audmux external port setup failed\n");
		return ret;
	}

	return 0;
}


static int imx_alc655_probe (struct platform_device *pdev) {
	struct device_node *ssi_np, *codec_np, *np = pdev->dev.of_node;

	struct platform_device *codec_pdev;
	struct platform_device *ssi_pdev;
	struct imx_alc655_data *data = NULL;
	struct snd_soc_dai_link_component *comp;
	int int_port, ext_port;
	int ret;

	ret = of_property_read_u32 (np, "mux-int-port", &int_port);
	if ( ret ) {
		dev_err (&pdev->dev, "mux-int-port property missing or invalid\n");
		return ret;
	}

	ret = of_property_read_u32 (np, "mux-ext-port", &ext_port);
	if ( ret ) {
		dev_err (&pdev->dev, "mux-ext-port property missing or invalid\n");
		return ret;
	}

	ret = imx_audmux_ac97_config (pdev, int_port, ext_port);
	if ( ret ) {
		dev_err (&pdev->dev, "Audmux port setup failed\n");
		return ret;
	}

	ssi_np = of_parse_phandle (np, "ssi-controller", 0);
	if ( !ssi_np ) {
		dev_err (&pdev->dev, "ssi-controller phandle missing or invalid\n");
		return -EINVAL;
	}
	ssi_pdev = of_find_device_by_node (ssi_np);
	if ( !ssi_pdev ) {
		dev_err (&pdev->dev, "Failed to find SSI platform device\n");
		ret = -EINVAL;
		goto fail;
	}

	codec_np = of_parse_phandle (np, "audio-codec", 0);
	if ( !codec_np ) {
		dev_err (&pdev->dev, "audio-codec phandle missing or invalid\n");
		ret = -EINVAL;
		goto fail;
	}
	codec_pdev = of_find_device_by_node (codec_np);
	if ( !codec_pdev ) {
		dev_err (&pdev->dev, "Failed to find codec device\n");
		ret = -EINVAL;
		goto fail;
	}

	data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
	if (!data) {
		ret = -ENOMEM;
		goto fail_data;
	}

	comp = devm_kzalloc(&pdev->dev, 3 * sizeof(*comp), GFP_KERNEL);
	if (!comp) {
		ret = -ENOMEM;
		goto fail_comp;
	}

	data->dai.cpus		= &comp[0];
	data->dai.codecs	= &comp[1];
	data->dai.platforms	= &comp[2];

	data->dai.num_cpus	= 1;
	data->dai.num_codecs	= 1;
	data->dai.num_platforms	= 1;

	data->dai.name =  "alc655-AC97";
	data->dai.stream_name = "ALC655-analog";
	data->dai.codecs->dai_name = "alc655-hifi-analog";
	data->dai.codecs->of_node = codec_np;
	data->dai.cpus->of_node = ssi_np;
	data->dai.platforms->of_node = ssi_np;
	data->dai.init = &imx_alc655_dai_init;
	//data->dai.ops = &imx_alc655_audio_params;

	data->card.dev = &pdev->dev;
	ret = snd_soc_of_parse_card_name(&data->card, "model");
	if (ret) {
		dev_err(&pdev->dev, "unable to parse model\n");
		goto fail;
	}
	// ret = snd_soc_of_parse_audio_routing(&data->card, "audio-routing");
	// if (ret)
	// 	goto fail;
	data->card.num_links = 1;
	data->card.owner = THIS_MODULE;
	data->card.dai_link = &data->dai;



	platform_set_drvdata(pdev, &data->card);
	snd_soc_card_set_drvdata(&data->card, data);

	ret = devm_snd_soc_register_card(&pdev->dev, &data->card);
	if (ret) {
		if (ret != -EPROBE_DEFER)
			dev_err(&pdev->dev, "snd_soc_register_card failed (%d)\n",
				ret);
		goto fail;
	}

	of_node_put(ssi_np);
	of_node_put(codec_np);

	return 0;
	// imx_alc655_dai.codecs->dai_name = dev_name (&codec_pdev->dev);
	// imx_alc655_dai.cpu_of_node = ssi_np;
	// imx_alc655_dai.cpus->dai_name = dev_name (&ssi_pdev->dev);
	// imx_alc655_dai.platforms->of_node = ssi_np;
	// imx_alc655_dai.ops = &imx_alc655_audio_ops;
	// imx_alc655_dai.dai_fmt = SND_SOC_DAIFMT_AC97 | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFS;

	// imx_alc655_card.dev = &pdev->dev;

	// platform_set_drvdata (pdev, &imx_alc655_card);

	// ret = snd_soc_register_card (&imx_alc655_card);
	// if ( ret )
	// 	dev_err (&pdev->dev, "snd_soc_register_card() failed: %d\n", ret);

	return 0;

fail_comp:
	kfree( data );
fail_data:
fail:
	if ( ssi_np )
		of_node_put (ssi_np);
	if ( codec_np )
		of_node_put (codec_np);

	return ret;
}


static int imx_alc655_remove (struct platform_device *pdev) {
	int ret;
	struct snd_soc_card *card = platform_get_drvdata (pdev);

	ret = snd_soc_unregister_card (card);

	return ret;
}


static const struct of_device_id imx_alc655_audio_match[] = {
	{ .compatible = "seco,imx-alc655-audio", },
	{}
};


MODULE_DEVICE_TABLE(of, imx_alc655_audio_match);


static struct platform_driver imx_alc655_driver = {
	.driver = {
		.name           = DRV_NAME,
		.owner          = THIS_MODULE,
		.of_match_table = imx_alc655_audio_match,
	},
	.probe  = imx_alc655_probe,
	.remove = imx_alc655_remove,
};
module_platform_driver(imx_alc655_driver);


MODULE_AUTHOR("Seco <info@seco.it>");
MODULE_DESCRIPTION(DRV_NAME ": Freescale i.MX ALC655 AC97 ASoC machine driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:imx-alc655");
