/*
 * Driver for SECO watchdog device controlled through GPIO-line
 *
 * Author: 2023, Gianfranco Mariotti <gianfranco.mariotti@seco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/err.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/gpio/consumer.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/watchdog.h>

struct gpio_wdt_priv {
	struct gpio_desc       *gpio_en;
	struct gpio_desc       *gpio_trg;
	struct watchdog_device wdd;
	bool                   state;
	bool                   enable_high;
	bool                   trigger_high;
};

static int gpio_wdt_ping(struct watchdog_device *wdd)
{
	struct gpio_wdt_priv *priv = watchdog_get_drvdata(wdd);

	/* Toggle trigger pin */
	priv->state = !priv->state;
	gpiod_set_value_cansleep(priv->gpio_trg, priv->state);

	return 0;
}

static int gpio_wdt_start(struct watchdog_device *wdd)
{
	struct gpio_wdt_priv *priv = watchdog_get_drvdata(wdd);

	gpiod_direction_output(priv->gpio_en, priv->enable_high);

	priv->state = priv->trigger_high;
	gpiod_direction_output(priv->gpio_trg, priv->state);

	set_bit(WDOG_HW_RUNNING, &wdd->status);

	return gpio_wdt_ping(wdd);
}

static int gpio_wdt_stop(struct watchdog_device *wdd)
{
	struct gpio_wdt_priv *priv = watchdog_get_drvdata(wdd);

	set_bit(WDOG_HW_RUNNING, &wdd->status);

	return 0;
}

static const struct watchdog_info gpio_wdt_ident = {
	.options	= WDIOF_MAGICCLOSE | WDIOF_KEEPALIVEPING |  WDIOF_SETTIMEOUT,
	.identity	= "SECO GPIO Watchdog",
};

static const struct watchdog_ops gpio_wdt_ops = {
	.owner		= THIS_MODULE,
	.start		= gpio_wdt_start,
	.stop		= gpio_wdt_stop,
	.ping		= gpio_wdt_ping,
};

static int gpio_wdt_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct gpio_wdt_priv *priv;
	enum gpiod_flags gflags;
	unsigned int hw_margin;
	unsigned int timeout;
	int ret;

	priv = devm_kzalloc(dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	platform_set_drvdata(pdev, priv);

	priv->gpio_en = devm_gpiod_get(dev, "enable", GPIOD_ASIS);
	if (IS_ERR(priv->gpio_en))
		return PTR_ERR(priv->gpio_en);
	priv->gpio_trg = devm_gpiod_get(dev, "trigger", GPIOD_ASIS);
	if (IS_ERR(priv->gpio_trg))
		return PTR_ERR(priv->gpio_trg);

	ret = of_property_read_u32(np, "hw_margin_ms", &hw_margin);
	if (ret)
		return ret;
	if (hw_margin < 2 || hw_margin > 65535)
		return -EINVAL;

	priv->enable_high  = of_property_read_bool(np, "enable_high");
	priv->trigger_high = of_property_read_bool(np, "trigger_high");

	watchdog_set_drvdata(&priv->wdd, priv);

	priv->wdd.info                = &gpio_wdt_ident;
	priv->wdd.ops                 = &gpio_wdt_ops;
	priv->wdd.min_timeout         = 1;
	priv->wdd.max_hw_heartbeat_ms = hw_margin;
	priv->wdd.parent              = dev;

	ret = of_property_read_u32(np, "timeout_s", &timeout);
	if (ret)
		priv->wdd.timeout = 60;
	else {
		if (timeout < 5 || timeout > 120)
			priv->wdd.timeout = 60;
		else
			priv->wdd.timeout = timeout;
	}

	watchdog_init_timeout(&priv->wdd, 0, &pdev->dev);

	watchdog_stop_on_reboot(&priv->wdd);

	gpio_wdt_start(&priv->wdd);

	ret = watchdog_register_device(&priv->wdd);

	return ret;
}

static int gpio_wdt_remove(struct platform_device *pdev)
{
	struct gpio_wdt_priv *priv = platform_get_drvdata(pdev);

	watchdog_unregister_device(&priv->wdd);

	return 0;
}

static const struct of_device_id gpio_wdt_dt_ids[] = {
	{ .compatible = "seco,wdt-gpio", },
	{ }
};
MODULE_DEVICE_TABLE(of, gpio_wdt_dt_ids);

static struct platform_driver gpio_wdt_driver = {
	.driver	= {
		.name		= "seco-gpio-wdt",
		.of_match_table	= gpio_wdt_dt_ids,
	},
	.probe	= gpio_wdt_probe,
	.remove	= gpio_wdt_remove,
};

#ifdef CONFIG_GPIO_WATCHDOG_ARCH_INITCALL
static int __init gpio_wdt_init(void)
{
	return platform_driver_register(&gpio_wdt_driver);
}
arch_initcall(gpio_wdt_init);
#else
module_platform_driver(gpio_wdt_driver);
#endif

MODULE_AUTHOR("Gianfranco Mariotti <gianfranco.mariotti@seco.com>");
MODULE_DESCRIPTION("SECO GPIO Watchdog");
MODULE_LICENSE("GPL");
