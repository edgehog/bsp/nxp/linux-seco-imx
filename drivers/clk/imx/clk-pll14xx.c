// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright 2017-2018 NXP.
 */

#include <linux/bits.h>
#include <linux/clk-provider.h>
#include <linux/err.h>
#include <linux/export.h>
#include <linux/io.h>
#include <linux/iopoll.h>
#include <linux/slab.h>
#include <linux/jiffies.h>

#include "clk.h"

#define GNRL_CTL	0x0
#define FDIV_CTL0	0x4
#define FDIV_CTL1	0x8
#define SSCG_CTL	0xC
#define LOCK_STATUS	BIT(31)
#define LOCK_SEL_MASK	BIT(29)
#define CLKE_MASK	BIT(11)
#define RST_MASK	BIT(9)
#define BYPASS_MASK	BIT(4)
#define MDIV_SHIFT	12
#define MDIV_MASK	GENMASK(21, 12)
#define PDIV_SHIFT	4
#define PDIV_MASK	GENMASK(9, 4)
#define SDIV_SHIFT	0
#define SDIV_MASK	GENMASK(2, 0)
#define KDIV_SHIFT	0
#define KDIV_MASK	GENMASK(15, 0)

#define LOCK_TIMEOUT_US		10000

struct clk_pll14xx {
	struct clk_hw			hw;
	void __iomem			*base;
	enum imx_pll14xx_type		type;
	const struct imx_pll14xx_rate_table *rate_table;
	int rate_count;
};

#define to_clk_pll14xx(_hw) container_of(_hw, struct clk_pll14xx, hw)

static const struct imx_pll14xx_rate_table imx_pll1416x_tbl[] = {
	PLL_1416X_RATE(1800000000U, 225, 3, 0),
	PLL_1416X_RATE(1600000000U, 200, 3, 0),
	PLL_1416X_RATE(1500000000U, 375, 3, 1),
	PLL_1416X_RATE(1400000000U, 350, 3, 1),
	PLL_1416X_RATE(1200000000U, 300, 3, 1),
	PLL_1416X_RATE(1000000000U, 250, 3, 1),
	PLL_1416X_RATE(800000000U,  200, 3, 1),
	PLL_1416X_RATE(750000000U,  250, 2, 2),
	PLL_1416X_RATE(700000000U,  350, 3, 2),
	PLL_1416X_RATE(600000000U,  300, 3, 2),
};

/* According to refmanual V1.0 the formula for the PLL is:
	(( m + k/65536 ) * Fin ) / ( p * pow(2, s))
	Fin is 24Mhz (mx8mp and mx8mm)
	The PLL_1443X_RATE macro parameters are:
	rate, m, p, s, k
*/  
static const struct imx_pll14xx_rate_table imx_pll1443x_tbl[] = {
	PLL_1443X_RATE(1039500000U, 173, 2, 1, 16384),
	PLL_1443X_RATE(995400000U, 622, 15, 0, 8192),
	PLL_1443X_RATE(987000000U, 329, 2, 2, 0),
	PLL_1443X_RATE(714000000U, 238, 2, 2, 0),
	PLL_1443X_RATE(650000000U, 325, 3, 2, 0),
	PLL_1443X_RATE(594000000U, 198, 2, 2, 0),
	PLL_1443X_RATE(519750000U, 173, 2, 2, 16384),
	PLL_1443X_RATE(393216000U, 262, 2, 3, 9437),
	PLL_1443X_RATE(361267200U, 361, 3, 3, 17511),
};

struct imx_pll14xx_clk imx_1443x_pll = {
	.type = PLL_1443X,
	.rate_table = imx_pll1443x_tbl,
	.rate_count = ARRAY_SIZE(imx_pll1443x_tbl),
};
EXPORT_SYMBOL_GPL(imx_1443x_pll);

struct imx_pll14xx_clk imx_1443x_dram_pll = {
	.type = PLL_1443X,
	.rate_table = imx_pll1443x_tbl,
	.rate_count = ARRAY_SIZE(imx_pll1443x_tbl),
	.flags = CLK_GET_RATE_NOCACHE,
};
EXPORT_SYMBOL_GPL(imx_1443x_dram_pll);

struct imx_pll14xx_clk imx_1416x_pll = {
	.type = PLL_1416X,
	.rate_table = imx_pll1416x_tbl,
	.rate_count = ARRAY_SIZE(imx_pll1416x_tbl),
};
EXPORT_SYMBOL_GPL(imx_1416x_pll);

static const struct imx_pll14xx_rate_table *imx_get_pll_settings(
		struct clk_pll14xx *pll, unsigned long rate)
{
	const struct imx_pll14xx_rate_table *rate_table = pll->rate_table;
	int i;

	for (i = 0; i < pll->rate_count; i++)
		if (rate == rate_table[i].rate)
			return &rate_table[i];

	return NULL;
}

static long clk_pll14xx_round_rate(struct clk_hw *hw, unsigned long rate,
			unsigned long *prate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	const struct imx_pll14xx_rate_table *rate_table = pll->rate_table;
	int i;

	/* Assumming rate_table is in descending order */
	for (i = 0; i < pll->rate_count; i++)
		if (rate >= rate_table[i].rate)
			return rate_table[i].rate;

	/* return minimum supported value */
	return rate_table[i - 1].rate;
}

static unsigned long clk_pll1416x_recalc_rate(struct clk_hw *hw,
						  unsigned long parent_rate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	u32 mdiv, pdiv, sdiv, pll_div;
	u64 fvco = parent_rate;

	pll_div = readl_relaxed(pll->base + FDIV_CTL0);
	mdiv = (pll_div & MDIV_MASK) >> MDIV_SHIFT;
	pdiv = (pll_div & PDIV_MASK) >> PDIV_SHIFT;
	sdiv = (pll_div & SDIV_MASK) >> SDIV_SHIFT;

	fvco *= mdiv;
	do_div(fvco, pdiv << sdiv);

	return fvco;
}

static unsigned long clk_pll1443x_recalc_rate(struct clk_hw *hw,
						  unsigned long parent_rate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	const struct imx_pll14xx_rate_table *rate_table = pll->rate_table;
	u32 mdiv, pdiv, sdiv, pll_div_ctl0, pll_div_ctl1;
	short int kdiv;
	u64 fvco = parent_rate;
	long rate = 0;
	int i;

	pll_div_ctl0 = readl_relaxed(pll->base + FDIV_CTL0);
	pll_div_ctl1 = readl_relaxed(pll->base + FDIV_CTL1);
	mdiv = (pll_div_ctl0 & MDIV_MASK) >> MDIV_SHIFT;
	pdiv = (pll_div_ctl0 & PDIV_MASK) >> PDIV_SHIFT;
	sdiv = (pll_div_ctl0 & SDIV_MASK) >> SDIV_SHIFT;
	kdiv = pll_div_ctl1 & KDIV_MASK;

	/*
	 * Sometimes, the recalculated rate has deviation due to
	 * the frac part. So find the accurate pll rate from the table
	 * first, if no match rate in the table, use the rate calculated
	 * from the equation below.
	 */
	for (i = 0; i < pll->rate_count; i++) {
		if (rate_table[i].pdiv == pdiv && rate_table[i].mdiv == mdiv &&
		    rate_table[i].sdiv == sdiv && rate_table[i].kdiv == kdiv)
			rate = rate_table[i].rate;
	}

	/* fvco = (m * 65536 + k) * Fin / (p * 65536) */
	fvco *= (mdiv * 65536 + kdiv);
	pdiv *= 65536;

	do_div(fvco, pdiv << sdiv);

	return rate ? (unsigned long) rate : (unsigned long)fvco;
}

static inline bool clk_pll14xx_mp_change(const struct imx_pll14xx_rate_table *rate,
					  u32 pll_div)
{
	u32 old_mdiv, old_pdiv;

	old_mdiv = (pll_div & MDIV_MASK) >> MDIV_SHIFT;
	old_pdiv = (pll_div & PDIV_MASK) >> PDIV_SHIFT;

	return rate->mdiv != old_mdiv || rate->pdiv != old_pdiv;
}

static int clk_pll14xx_wait_lock(struct clk_pll14xx *pll)
{
	u32 val;

	return readl_poll_timeout(pll->base, val, val & LOCK_STATUS, 0,
			LOCK_TIMEOUT_US);
}

static int clk_pll1416x_set_rate(struct clk_hw *hw, unsigned long drate,
				 unsigned long prate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	const struct imx_pll14xx_rate_table *rate;
	u32 tmp, div_val;
	int ret;

	rate = imx_get_pll_settings(pll, drate);
	if (!rate) {
		pr_err("%s: Invalid rate : %lu for pll clk %s\n", __func__,
		       drate, clk_hw_get_name(hw));
		return -EINVAL;
	}

	tmp = readl_relaxed(pll->base + FDIV_CTL0);

	if (!clk_pll14xx_mp_change(rate, tmp)) {
		tmp &= ~(SDIV_MASK) << SDIV_SHIFT;
		tmp |= rate->sdiv << SDIV_SHIFT;
		writel_relaxed(tmp, pll->base + FDIV_CTL0);

		return 0;
	}

	/* Bypass clock and set lock to pll output lock */
	tmp = readl_relaxed(pll->base);
	tmp |= LOCK_SEL_MASK;
	writel_relaxed(tmp, pll->base);

	/* Enable RST */
	tmp &= ~RST_MASK;
	writel_relaxed(tmp, pll->base);

	/* Enable BYPASS */
	tmp |= BYPASS_MASK;
	writel(tmp, pll->base);

	div_val = (rate->mdiv << MDIV_SHIFT) | (rate->pdiv << PDIV_SHIFT) |
		(rate->sdiv << SDIV_SHIFT);
	writel_relaxed(div_val, pll->base + FDIV_CTL0);

	/*
	 * According to SPEC, t3 - t2 need to be greater than
	 * 1us and 1/FREF, respectively.
	 * FREF is FIN / Prediv, the prediv is [1, 63], so choose
	 * 3us.
	 */
	udelay(3);

	/* Disable RST */
	tmp |= RST_MASK;
	writel_relaxed(tmp, pll->base);

	/* Wait Lock */
	ret = clk_pll14xx_wait_lock(pll);
	if (ret)
		return ret;

	/* Bypass */
	tmp &= ~BYPASS_MASK;
	writel_relaxed(tmp, pll->base);

	return 0;
}

static int clk_pll1443x_set_rate(struct clk_hw *hw, unsigned long drate,
				 unsigned long prate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	const struct imx_pll14xx_rate_table *rate;
	u32 tmp, div_val;
	u32 sscg_en, mfr, mrr, sel_pf, sscg_val;
	u32 mfr_mrr, mf, mr;
	struct device_node *np;
	const char *clk_name;
	char property[40];
	u32 readbuf;
	int ret;

	rate = imx_get_pll_settings(pll, drate);
	if (!rate) {
		pr_err("%s: Invalid rate : %lu for pll clk %s\n", __func__,
			drate, clk_hw_get_name(hw));
		return -EINVAL;
	}

	tmp = readl_relaxed(pll->base + FDIV_CTL0);

	if (!clk_pll14xx_mp_change(rate, tmp)) {
		tmp &= ~(SDIV_MASK) << SDIV_SHIFT;
		tmp |= rate->sdiv << SDIV_SHIFT;
		writel_relaxed(tmp, pll->base + FDIV_CTL0);

		tmp = rate->kdiv << KDIV_SHIFT;
		writel_relaxed(tmp, pll->base + FDIV_CTL1);

		return 0;
	}

	/* Enable RST */
	tmp = readl_relaxed(pll->base);
	tmp &= ~RST_MASK;
	writel_relaxed(tmp, pll->base);

	/* Enable BYPASS */
	tmp |= BYPASS_MASK;
	writel_relaxed(tmp, pll->base);

	div_val = (rate->mdiv << MDIV_SHIFT) | (rate->pdiv << PDIV_SHIFT) |
		(rate->sdiv << SDIV_SHIFT);
	writel_relaxed(div_val, pll->base + FDIV_CTL0);
	writel_relaxed(rate->kdiv << KDIV_SHIFT, pll->base + FDIV_CTL1);

	/* Enable SSCG */
	np = of_find_compatible_node(NULL, NULL, "fsl,imx8mm-anatop");
	if (np) {
		clk_name = clk_hw_get_name(hw);

		sprintf(property, "anatop-%s,sscg-enable", clk_name);
		if (of_property_read_bool(np, property)) {
			sscg_en = 1;
			mfr = mrr = sel_pf = -1;

			/* 0 <= mfr <= 255, 1 <= mrr <= 63, 0 <= mrr * mfr <= 512 */
			sprintf(property, "anatop-%s,mfr", clk_name);
			if (!of_property_read_u32(np, property, &readbuf)) {
				mfr = readbuf;
				if (!(mfr >= 0 && mfr <= 255)) {
					sscg_en = 0;
					pr_err("%s: SSCG not enabled for pll clk %s: mfr %d is out of range\n",
							__func__, clk_name, mfr);
				}
			} else {
				sscg_en = 0;
				pr_err("%s: SSCG not enabled for pll clk %s: unable to read mfr\n",
						__func__, clk_name);
			}
			sprintf(property, "anatop-%s,mrr", clk_name);
			if (!of_property_read_u32(np, property, &readbuf)) {
				mrr = readbuf;
				if (!(mrr >= 1 && mrr <= 63)) {
					sscg_en = 0;
					pr_err("%s: SSCG not enabled for pll clk %s: mrr %d is out of range\n",
							__func__, clk_name, mrr);
				}
				mfr_mrr = mfr * mrr;
				if (!(mfr_mrr >= 0 && mfr_mrr <= 512)) {
					sscg_en = 0;
					pr_err("%s: SSCG not enabled for pll clk %s: mfr * mrr %d is out of range\n",
							__func__, clk_name, mfr_mrr);
				}
			} else {
				sscg_en = 0;
				pr_err("%s: SSCG not enabled for pll clk %s: unable to read mrr\n",
						__func__, clk_name);
			}
			/* sel_pf >= 0 && sel_pf <= 2 */
			sprintf(property, "anatop-%s,sel-pf", clk_name);
			if (!of_property_read_u32(np, property, &readbuf)) {
				sel_pf = readbuf;
				if (!(sel_pf >= 0 && sel_pf <= 2)) {
					sscg_en = 0;
					pr_err("%s: SSCG not enabled for pll clk %s: sel-pf %d is out of range\n",
							__func__, clk_name, sel_pf);
				}
			} else {
				sscg_en = 0;
				pr_err("%s: SSCG not enabled for pll clk %s: unable to read sel-pf\n",
						__func__, clk_name);
			}

			if (sscg_en) {
				sscg_val = readl_relaxed(pll->base + SSCG_CTL);
				sscg_val |= 1 << 31;   //SSCG_EN
				sscg_val |= mfr << 12; //PLL_MFREQ_CTL
				sscg_val |= mrr << 4;  //PLL_MRAT_CTL
				sscg_val |= sel_pf;    //SEL_PF
				writel_relaxed(sscg_val, pll->base + SSCG_CTL);

				/* MF = Fin / p / mfr / (2^5) [Hz] */
				mf = prate / (rate->pdiv * mfr * 32);
				/* MR = mfr * mrr / m / (2^6) * 100 [%] */
				mr = mfr_mrr * 1000 / (rate->mdiv * 64);
				pr_info("%s: SSCG enabled for pll clk %s: MF: %dHz, MR: %d.%d%%\n",
						__func__, clk_name, mf, mr / 10, mr % 10);
			}
		}
	}

	/*
	 * According to SPEC, t3 - t2 need to be greater than
	 * 1us and 1/FREF, respectively.
	 * FREF is FIN / Prediv, the prediv is [1, 63], so choose
	 * 3us.
	 */
	udelay(3);

	/* Disable RST */
	tmp |= RST_MASK;
	writel_relaxed(tmp, pll->base);

	/* Wait Lock*/
	ret = clk_pll14xx_wait_lock(pll);
	if (ret)
		return ret;

	/* Bypass */
	tmp &= ~BYPASS_MASK;
	writel_relaxed(tmp, pll->base);

	return 0;
}

static int clk_pll14xx_prepare(struct clk_hw *hw)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	u32 val;
	int ret;

	/*
	 * RESETB = 1 from 0, PLL starts its normal
	 * operation after lock time
	 */
	val = readl_relaxed(pll->base + GNRL_CTL);
	if (val & RST_MASK)
		return 0;
	val |= BYPASS_MASK;
	writel_relaxed(val, pll->base + GNRL_CTL);
	val |= RST_MASK;
	writel_relaxed(val, pll->base + GNRL_CTL);

	ret = clk_pll14xx_wait_lock(pll);
	if (ret)
		return ret;

	val &= ~BYPASS_MASK;
	writel_relaxed(val, pll->base + GNRL_CTL);

	return 0;
}

static int clk_pll14xx_is_prepared(struct clk_hw *hw)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	u32 val;

	val = readl_relaxed(pll->base + GNRL_CTL);

	return (val & RST_MASK) ? 1 : 0;
}

static void clk_pll14xx_unprepare(struct clk_hw *hw)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	u32 val;

	/*
	 * Set RST to 0, power down mode is enabled and
	 * every digital block is reset
	 */
	val = readl_relaxed(pll->base + GNRL_CTL);
	val &= ~RST_MASK;
	writel_relaxed(val, pll->base + GNRL_CTL);
}

void clk_set_delta_k(struct clk_hw *hw, short int delta_k)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);
	short int k;
	u32 val;

	val = readl_relaxed(pll->base + FDIV_CTL1);
	k = (val & KDIV_MASK) + delta_k;
	writel_relaxed(k << KDIV_SHIFT, pll->base + FDIV_CTL1);
}

void clk_get_pll_setting(struct clk_hw *hw, u32 *pll_div_ctrl0,
	u32 *pll_div_ctrl1)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(hw);

	*pll_div_ctrl0 = readl_relaxed(pll->base + FDIV_CTL0);
	*pll_div_ctrl1 = readl_relaxed(pll->base + FDIV_CTL1);
}

static const struct clk_ops clk_pll1416x_ops = {
	.prepare	= clk_pll14xx_prepare,
	.unprepare	= clk_pll14xx_unprepare,
	.is_prepared	= clk_pll14xx_is_prepared,
	.recalc_rate	= clk_pll1416x_recalc_rate,
	.round_rate	= clk_pll14xx_round_rate,
	.set_rate	= clk_pll1416x_set_rate,
};

static const struct clk_ops clk_pll1416x_min_ops = {
	.recalc_rate	= clk_pll1416x_recalc_rate,
};

static const struct clk_ops clk_pll1443x_ops = {
	.prepare	= clk_pll14xx_prepare,
	.unprepare	= clk_pll14xx_unprepare,
	.is_prepared	= clk_pll14xx_is_prepared,
	.recalc_rate	= clk_pll1443x_recalc_rate,
	.round_rate	= clk_pll14xx_round_rate,
	.set_rate	= clk_pll1443x_set_rate,
};

struct clk_hw *imx_dev_clk_hw_pll14xx(struct device *dev, const char *name,
				const char *parent_name, void __iomem *base,
				const struct imx_pll14xx_clk *pll_clk)
{
	struct clk_pll14xx *pll;
	struct clk_hw *hw;
	struct clk_init_data init;
	int ret;
	u32 val;

	pll = kzalloc(sizeof(*pll), GFP_KERNEL);
	if (!pll)
		return ERR_PTR(-ENOMEM);

	init.name = name;
	init.flags = pll_clk->flags;
	init.parent_names = &parent_name;
	init.num_parents = 1;

	switch (pll_clk->type) {
	case PLL_1416X:
		if (!pll_clk->rate_table)
			init.ops = &clk_pll1416x_min_ops;
		else
			init.ops = &clk_pll1416x_ops;
		break;
	case PLL_1443X:
		init.ops = &clk_pll1443x_ops;
		break;
	default:
		pr_err("%s: Unknown pll type for pll clk %s\n",
		       __func__, name);
		kfree(pll);
		return ERR_PTR(-EINVAL);
	};

	pll->base = base;
	pll->hw.init = &init;
	pll->type = pll_clk->type;
	pll->rate_table = pll_clk->rate_table;
	pll->rate_count = pll_clk->rate_count;

	val = readl_relaxed(pll->base + GNRL_CTL);
	val &= ~BYPASS_MASK;
	writel_relaxed(val, pll->base + GNRL_CTL);

	hw = &pll->hw;

	ret = clk_hw_register(dev, hw);
	if (ret) {
		pr_err("%s: failed to register pll %s %d\n",
			__func__, name, ret);
		kfree(pll);
		return ERR_PTR(ret);
	}

	return hw;
}
EXPORT_SYMBOL_GPL(imx_dev_clk_hw_pll14xx);
