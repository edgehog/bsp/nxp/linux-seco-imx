/*
 * Copyright 2021 SECO
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/display/media-bus-format.h>
#include <dt-bindings/display/drm-mode-connector.h>

/dts-v1/;
/plugin/;

/ {
	compatible = "fsl,imx8mp-evk", "seco,imx8mp-d18", "fsl,imx8mp";

/*  __________________________________________________________________________
 * |                                                                          |
 * |                               LVDS                                       |
 * |__________________________________________________________________________|
 */

	fragment@0 {
		target-path = "/";
		__overlay__ {
			lcd0_panel: lcd0_panel {
				compatible = "seco-auo,p215hvn01", "panel-dpi";
				backlight = <&lcd0_backlight>;
				power-supply = <&reg_lcd0_vdd_en>;

				width-mm  = <344>;
				height-mm = <194>;

				bpc = <8>;
				bus-format = <DT_MEDIA_BUS_FMT_RGB888_1X7X4_SPWG>;
				connector-type = <DT_DRM_MODE_CONNECTOR_LVDS>;

				status = "okay";

				panel-timing {
					clock-frequency = <148500000>;
					hactive = <1920>;
					vactive = <1080>;
					hfront-porch = <100>;
					hback-porch = <120>;
					hsync-len = <60>;
					vback-porch = <30>;
					vfront-porch = <10>;
					vsync-len = <5>;
				};

				port {
					lcd0_panel_in: endpoint {
						remote-endpoint = <&lcd0_out>;
					};
				};
			};
		};
	};

	fragment@1 {
		target = <&ldb>;
		__overlay__ {
			status = "okay";

			fsl,dual-channel;

			lvds-channel@0 {
				status = "okay";

				port@1 {
					reg = <1>;

					lcd0_out: endpoint {
						remote-endpoint = <&lcd0_panel_in>;
					};
				};
			};
		};
	};

	fragment@2 {
		target = <&lcdif2>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@3 {
		target = <&ldb_phy>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@4 {
		target = <&reg_lcd0_vdd_en>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@5 {
		target = <&lcd0_backlight>;
		__overlay__ {
			status = "okay";

			brightness-levels = <0 100>;
			num-interpolated-steps = <100>;
			default-brightness-level = <80>;
		};
	};

};
