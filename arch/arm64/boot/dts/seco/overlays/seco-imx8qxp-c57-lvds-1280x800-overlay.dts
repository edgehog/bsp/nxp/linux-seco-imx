/*
 * Copyright 2017-2018 NXP
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dt-bindings/display/media-bus-format.h>
#include <dt-bindings/gpio/gpio.h>

/dts-v1/;
/plugin/;

/ {
    compatible = "fsl,imx8qxp-mek","seco,imx8qxp-c57","fsl,imx8qxp";

/*  __________________________________________________________________________
 * |                                                                          |
 * |                      LVDS 1280x800 (SINGLE CHANNEL)                      |
 * |__________________________________________________________________________|
 */

    fragment@0 {
        target-path = "/";
        __overlay__ {
            regulators {
                mux_sel: mux_sel {
                    compatible = "regulator-fixed";
                    regulator-name = "MUX_SEL";
                    regulator-min-microvolt = <3300000>;
                    regulator-max-microvolt = <3300000>;
                    gpio = <&gpio3 22 GPIO_ACTIVE_HIGH>;
                    regulator-always-on;
                };

                reg_backlight_vcc_bkl_sw: backlight_vcc_bkl_sw {
                        compatible = "regulator-fixed";
                        regulator-name = "backlight_vcc_bkl_sw";
                        regulator-min-microvolt = <3300000>;
                        regulator-max-microvolt = <3300000>;
                        /* The gpio field is configured by u-boot depending on
                        the revision of the board. The gpio declared here is
                        related to the latest hardware revision. */
                        gpio = <&gpio3 3 GPIO_ACTIVE_HIGH>;
                        enable-active-high;
                        regulator-boot-on;
                };

                reg_en_vcc_lcd_sw: en_vcc_lcd {
                        compatible = "regulator-fixed";
                        regulator-name = "EN_VCC_LCD";
                        regulator-min-microvolt = <3300000>;
                        regulator-max-microvolt = <3300000>;
                        gpio = <&gpio0 12 GPIO_ACTIVE_HIGH>;
                        enable-active-high;
                        regulator-boot-on;
                };
            };
        };
    };

    fragment@1 {
        target-path = "/";
        __overlay__ {
            /* Both Panels backlight + MP3385AGR */
            backlight0: lvds_backlight0 {
                compatible = "mp3385-backlight";
                pwms = <&pwm_mipi_lvds0 0 100000 0>;

                brightness-levels = < 0  1  2  3  4  5  6  7  8  9
                            10 11 12 13 14 15 16 17 18 19
                            20 21 22 23 24 25 26 27 28 29
                            30 31 32 33 34 35 36 37 38 39
                            40 41 42 43 44 45 46 47 48 49
                            50 51 52 53 54 55 56 57 58 59
                            60 61 62 63 64 65 66 67 68 69
                            70 71 72 73 74 75 76 77 78 79
                            80 81 82 83 84 85 86 87 88 89
                            90 91 92 93 94 95 96 97 98 99
                            100>;
                default-brightness-level = <90>;
                client-device  = <&mp3385_led_driver>;
                enable-gpios = <&gpio3 1 GPIO_ACTIVE_HIGH>;
                power-supply = <&reg_backlight_vcc_bkl_sw>;
            };

            /* Backlight on CN25 (pin30) */
            lvds_backlight1 {
                compatible = "pwm-backlight";
                pwms = <&pwm_mipi_lvds1 0 100000 0>;

                brightness-levels = < 0  1  2  3  4  5  6  7  8  9
                            10 11 12 13 14 15 16 17 18 19
                            20 21 22 23 24 25 26 27 28 29
                            30 31 32 33 34 35 36 37 38 39
                            40 41 42 43 44 45 46 47 48 49
                            50 51 52 53 54 55 56 57 58 59
                            60 61 62 63 64 65 66 67 68 69
                            70 71 72 73 74 75 76 77 78 79
                            80 81 82 83 84 85 86 87 88 89
                            90 91 92 93 94 95 96 97 98 99
                            100>;
                default-brightness-level = <90>;
                enable-gpios = <&gpio0 13 GPIO_ACTIVE_HIGH>;
                power-supply = <&reg_backlight_vcc_bkl_sw>;
            };

            lvds_panel {
                compatible   = "panel-lvds";
                status = "okay";
                data-mapping = "vesa-24";
                width-mm = <217>;
                height-mm = <136>;
                backlight = <&backlight0>;
                power-supply = <&reg_en_vcc_lcd_sw>;
                enable-gpio = <&gpio0 15 GPIO_ACTIVE_HIGH>;

                panel-timing {
                    clock-frequency = <66100000>;
                    hactive = <1280>;
                    vactive = <800>;
                    hback-porch = <35>;
                    hfront-porch = <35>;
                    vback-porch = <4>;
                    vfront-porch = <4>;
                    hsync-len = <10>;
                    vsync-len = <2>;
                    de-active = <0>;
                    pixelclk-active = <0>;
                };

                port@0 {
                    panel_lvds_in: endpoint {
                        remote-endpoint = <&lvds_out>;
                    };
                };
            };
        };
    };

    fragment@2 {
            target = <&ldb1_phy>;
            __overlay__ {
        status = "okay";
        };
    };

    fragment@3 {
        target = <&ldb1>;
        __overlay__ {
            #address-cells = <1>;
            #size-cells = <0>;
            status = "okay";

            lvds-channel@0 {
                #address-cells = <1>;
                #size-cells = <0>;
                status = "okay";

                /delete-node/ port@1;

                port@1 {
                    reg = <1>;
                    #address-cells = <1>;
                    #size-cells = <0>;
                    lvds_out: endpoint {
                        remote-endpoint = <&panel_lvds_in>;
                    };
                };
            };
        };
    };

    fragment@4 {
        target = <&ldb2_phy>;
        __overlay__ {
            status = "disabled";
        };
    };

    fragment@5 {
        target = <&ldb2>;
        __overlay__ {
            status = "disabled";
        };
    };
};