/*
 * Copyright 2017-2018 SECO
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dt-bindings/drm_mipi_dsi.h>

/dts-v1/;
/plugin/;

/ {
	compatible = "fsl,imx8mq-evk", "seco,seco-imx8mq-c12", "fsl,imx8mq";

/*  __________________________________________________________________________
 * |                                                                          |
 * |                               LCDIF LVDS                                 |
 * |__________________________________________________________________________|
 */

	fragment@0 {
		target = <&vpu>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@1 {
		target = <&vpu_v4l2>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@2 {
		target = <&gpu3d>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@3 {
		target = <&panel_lvds>;
		__overlay__ {
			#address-cells = <1>;
			#size-cells = <0>;
			status = "okay";

			port@0 {
				reg = <0>;
				panel_in: endpoint@0 {
					remote-endpoint = <&bridge_to_panel>;
				};
			};
		};
	};

	fragment@4 {
		target = <&dcss>;
		__overlay__ {
			status = "disabled";
		};
	};

	fragment@5 {
		target = <&lcdif>;
		__overlay__ {
			status = "okay";
			max-res = <1920>,<1080>;
		};
	};

	fragment@6 {
		target = <&adv_bridge>;
		__overlay__ {
			#address-cells = <1>;
			#size-cells = <0>;
			lvds,datamap = "jeida";
			lvds,dual-channel;
			lvds,preserve-dsi-timings;

			dsi,mode-flags = <(MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_BURST)>;
			clocks = <&si5351a 0>;
			status = "okay";

			port@0 {
				reg = <0>;
				sn65dsi84_in: endpoint@0 {
					remote-endpoint = <&mipi_dsi_out>;
				};
			};

			port@1 {
				reg = <1>;
				bridge_to_panel: endpoint@1 {
					remote-endpoint = <&panel_in>;
				};
			};
		};
	};

	fragment@7 {
		target = <&mipi_dsi>;
		__overlay__ {
			status = "okay";
			as_bridge;
			sync-pol = <1>;

			ports {
				#address-cells = <1>;
				#size-cells = <0>;

				port@1 {
					reg = <1>;
					#address-cells = <1>;
					#size-cells = <0>;
					mipi_dsi_out: endpoint {
						remote-endpoint = <&sn65dsi84_in>;
					};
				};
			};
		};
	};

	fragment@8 {
		target = <&dphy>;
		__overlay__ {
			status = "okay";
		};
	};

	/* RevD */
	fragment@9 {
		target = <&si5351a>;
		__overlay__ {
			status = "okay";
			clkout0 {
				silabs,pll-spread_spectrum-1_5;
			};
		};
	};

};
