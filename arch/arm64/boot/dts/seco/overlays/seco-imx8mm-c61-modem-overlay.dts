/*
 * Copyright 2017 NXP
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dt-bindings/gpio/gpio.h>

/dts-v1/;
/plugin/;

/ {
	compatible = "fsl,imx8mm-evk", "seco,imx8mm-c61", "fsl,imx8mm";

/*  __________________________________________________________________________
 * |                                                                          |
 * |                                 MODEM                                    |
 * |__________________________________________________________________________|
 */

	fragment@modem {
		target-path = "/";
		__overlay__ {
			reg_gsm_en: reggsmen {
				compatible = "regulator-fixed";
				regulator-name = "GSM_EN";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				gpio = <&gpio3 25 GPIO_ACTIVE_HIGH>;
				enable-active-high;
				regulator-boot-on;
				regulator-always-on;
			};

			usb_uab_switch_on: usbuabon {
				compatible = "regulator-fixed";
				regulator-name = "USB_UAB_SWITCH_ON";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				gpio = <&pca6416_20 10 GPIO_ACTIVE_LOW>;
				enable-active-low;
				regulator-boot-on;
				regulator-always-on;
			};

			gsm_modem: gsmmodem {
				compatible = "seco,gsm-modem";
				pinctrl-names = "default";
				pinctrl-0     = <&pinctrl_gsm_modem>;
				rst-gpios = <&gpio4 3 GPIO_ACTIVE_LOW>;
				usben-gpios = <&gpio4 12 GPIO_ACTIVE_LOW>;
				vgsm-gpios = <&pca6416_20 2 GPIO_ACTIVE_HIGH>;
				pwrkey-gpios = <&gpio4 18 GPIO_ACTIVE_HIGH>;
				status = "okay";
			};
		};
	};

};
