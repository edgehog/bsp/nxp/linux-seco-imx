/*
 * Copyright 2024 SECO
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dt-bindings/clock/imx8mp-clock.h>
#include <dt-bindings/gpio/gpio.h>
#include "../include/imx8mp-pinfunc.h"

/dts-v1/;
/plugin/;

/*  __________________________________________________________________________
 * |                                                                          |
 * |                            CARRIER BOARD E71                             |
 * |__________________________________________________________________________|
 */

&{/} {
	model = "SECO i.MX8MPlus LPDDR4 D18 on E71 carrier board";

	reg_audio: regulator-soc-audio {
		compatible = "regulator-fixed";
		regulator-name = "tlv320aic32x4_supply";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-always-on;
	};

	rs_232_en_b_reg: rs_232_en_b_reg {
		compatible = "regulator-fixed";
		regulator-name = "rs_232_en_b_reg";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		enable-active-high;
		gpio = <&pca9500_gpio 1 GPIO_ACTIVE_HIGH>;
	};
	rs_232_en_b_reg_consumer: RS_232_EN_B {
		compatible          = "reg-userspace-consumer";
		regulator-name      = "RS_232_EN_B";
		regulator-supplies  = "rs_232_en_b_reg";
		rs_232_en_b_reg-supply = <&rs_232_en_b_reg>;
	};

	sound {
		status                            = "okay";
		compatible                        = "simple-audio-card";
		simple-audio-card,format          = "i2s";
		simple-audio-card,name            = "tlv320-e71";
		simple-audio-card,frame-master    = <&cpudai>;
		simple-audio-card,bitclock-master = <&cpudai>;

		cpudai: simple-audio-card,cpu {
			sound-dai          = <&sai2>;
			dai-tdm-slot-num   = <2>;
			dai-tdm-slot-width = <16>;
		};
		simple-audio-card,codec {
			sound-dai = <&tlvcodec 0>;
		};
	};

	connector {
		compatible = "gpio-usb-b-connector", "usb-b-connector";
		label = "micro-USB";
		type = "micro";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_usb_1_id>;
		id-gpios = <&gpio1 10 GPIO_ACTIVE_HIGH>;
		vbus-supply = <&reg_usb1_host_vbus>;

		port {
			usb_ep: endpoint {
				remote-endpoint = <&usb_otg_ep>;
			};
		};
	};
};

&i2c1 {
	pca9500_gpio: gpio@24 {
		compatible = "nxp,pcf8574";
		reg = <0x24>;
		gpio-controller;
		#gpio-cells = <2>;

		gpio-line-names = "TS_RST", "RS_232_EN#", "GPIO2", "GPIO3", "FLASH_WP#", "GPIO7", "S3_LED", "S5_LED";
	};

	pca9500_eeprom: eeprom@54 {
		compatible = "atmel,24c02";
		reg = <0x54>;
		pagesize = <16>;
	};
};

&i2c5 {
	i2c-mux-tca9545@70 {
		compatible = "nxp,pca9545";
		reg = <0x70>;
		#address-cells = <1>;
		#size-cells = <0>;

		i2c_exp0: i2c@0 {
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0>;
		};

		i2c_exp1: i2c@1 {
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <1>;

			tlvcodec: tlv320aic32x4@18 {
				#sound-dai-cells = <0>;
				status = "okay";
				compatible = "ti,tlv320aic32x4";
				reg = <0x18>;
				reset-gpios = <&pca6416 8 GPIO_ACTIVE_LOW>;
				ldoin-supply = <&reg_audio>;
				iov-supply = <&reg_audio>;
				clocks = <&clk IMX8MP_CLK_SAI2_ROOT>;
				clock-names = "mclk";
				mic-bias;
			};
		};

		i2c_exp2: i2c@2 {
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <2>;
		};

		i2c_exp3: i2c@3 {
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <3>;
		};
	};
};

&flexspi {
	flash@0 {
		compatible = "micron,mt25qu256a", "jedec,spi-nor";
		reg = <0>;
		#address-cells = <1>;
		#size-cells = <1>;
		spi-max-frequency = <25000000>;
		spi-rx-bus-width = <4>;
		spi-tx-bus-width = <4>;
	};
};

&sai2 {
	assigned-clocks = <&clk IMX8MP_CLK_SAI2>;
	assigned-clock-parents = <&clk IMX8MP_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <49152000>;
	status = "okay";
};

&usb_dwc3_0 {
	usb-role-switch;
	role-switch-default-mode = "peripheral";
	port {
		usb_otg_ep: endpoint {
			remote-endpoint = <&usb_ep>;
		};
	};
};

&{/} {
	reg_lcd0_vdd_en: regulator-lcd0-vdd-en {
		compatible = "regulator-fixed";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_lcd0_vdd>;
		regulator-name = "lcd0_vdd_en";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		// polarity actually is driven via enable-active-high
		gpio = <&gpio4 21 GPIO_ACTIVE_LOW>;
		status = "disabled";
	};

	lcd0_backlight: lcd0_backlight {
		compatible = "pwm-backlight";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_lcd0_bklt>;
		pwms = <&pwm1 0 100000 0>;
		enable-gpios= <&gpio1 6 GPIO_ACTIVE_HIGH>;
		status = "disabled";
	};
};

&iomuxc {
	pinctrl_lcd0_vdd: lcd0vddgrp {
		fsl,pins = <
			MX8MP_IOMUXC_SAI2_RXFS__GPIO4_IO21		0x0 /* LCD0_VDD_EN */
		>;
	};

	pinctrl_lcd0_bklt: lcd0bkltgrp {
		fsl,pins = <
			MX8MP_IOMUXC_GPIO1_IO06__GPIO1_IO06		0x0 /* LCD0_BKLT_EN */
		>;
	};
};
